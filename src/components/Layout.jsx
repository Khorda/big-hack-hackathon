import React from 'react'
import {NavLink} from 'react-router-dom'

import '../App.scss';
import 'primereact/resources/themes/luna-pink/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

import { Toast } from 'primereact/toast';

const Header = () => (
    <div className="p-col-12 p-d-flex p-jc-around p-jc-md-center p-ai-center p-ai-md-end  " style={{ height: '100px' }}>
        <NavLink to='/profile' className="p-py-1 p-mx-1 p-px-1 p-mx-md-3 p-px-md-3 nav-link" activeClassName="p-shadow-6 bg-half-white">
            <i className='pi pi-user nav-icon'></i>
            <div>Profilo</div>
        </NavLink>
        <NavLink to='/share' className="p-py-1 p-mx-1 p-px-1 p-mx-md-3 p-px-md-3 nav-link" activeClassName="p-shadow-6 bg-half-white">
            <i className='pi pi-globe nav-icon'></i>
            <div>Storie</div>
        </NavLink>
        <NavLink to='/doctors' className="p-py-1 p-mx-1 p-px-1 p-mx-md-3 p-px-md-3 nav-link" activeClassName="p-shadow-6 bg-half-white">
            <i className='pi pi-heart nav-icon'></i>
            <div>Supporto</div>
        </NavLink>
    </div>
)

export default function Layout({ children, toastRef, showContent }) {
    return (
        <div className="App bg-bubbles">

            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>
            <div className="bubble"></div>


            <Toast ref={toastRef} position='top-right'></Toast>

            { showContent ? (
                <div className="p-grid p-dir-col-rev p-flex-md-column p-flex-nowrap h-100 p-mt-0 ">
                    <Header />
                    <div className="p-col-12 p-py-2 p-px-3 p-p-md-6 " style={{ height: 'calc(100% - 100px)' }}>
                        <div className="p-shadow-12 h-100 bg-half-white">
                            <div className="p-grid  p-py-6 p-px-1 p-p-md-6 p-mt-0 h-100" style={{ overflow: 'auto', position: "relative" }}>
                                {children}
                            </div>
                        </div>
                    </div>
                </div>

            ) : null}
        </div>
    )
}
