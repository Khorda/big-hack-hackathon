import React, { useRef, useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Layout from './components/Layout'
import { ACCEPT, REFUSE } from './views/Dialogs'
import { LoginDialog, RegisterDialog } from './views/Dialogs'
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Avatar } from 'primereact/avatar';
import { Divider } from 'primereact/divider';
import { Calendar } from 'primereact/calendar';


import PrimeReact from 'primereact/api';

import User from './img/user.png'
import Doc from './img/doctor.jpeg'

function App() {
  const [logged, setLogged] = useState(false);
  const [registering, setRegistering] = useState(false);
  const toastRef = useRef(null)
  const [profile, setProfile] = useState({
    nome: "Mary",
    email: "utente.prova@email.com",
    bio: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus!",
    "istologia & immunofenotipo": "Lorem",
    "tipo intervento": "Ipsum",
    "data intervento": "19 Ottobre 2020",
    "tipo chemioterapia / farmaci": "Nessuno",
    "data inizio chemioterapia / farmaci": "Nessuna",
    "data fine chemioterapia / farmaci": "Nessuna",
    "tipo radioterapia": "Amet",
    "data inizio radioterapia": "10 Ottobre 2020",
    "data fine radioterapia": "20 Dicembre 2020",
    "tipo ormonoterapia": "Amet",
    "data inizio ormonoterapia": "10 Ottobre 2020",
    "data fine ormonoterapia": "20 Dicembre 2020",
    "supporto psico-oncologico": "Psicologia comportamentale",
    "medico di riferimento": 'Mario Rossi'

  })
  const [followups] = useState([{
    data: 'X Maggio 20YZ',
    procedura: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus',
    operatore: "Luigi Verdi"
  }])
  const [stories] = useState([
    { nome: 'Maggie', eta: '35', stato: 'A', titolo: "La mia storia", storia: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus!" },
    { nome: 'Lucy', eta: '69', stato: 'B', titolo: "Rinascita", storia: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus!" },

    { nome: 'Grace', eta: '46', stato: 'C', titolo: "Coraggio!", storia: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus!" },
    { nome: 'Sandy', eta: '46', stato: 'C', titolo: "Coraggio!", storia: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus!" },
    { nome: 'Philo', eta: '46', stato: 'C', titolo: "Coraggio!", storia: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus!" },
    { nome: 'Ayko', eta: '46', stato: 'C', titolo: "Coraggio!", storia: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus! Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit maxime at quia et earum fuga ut quod facilis incidunt expedita neque molestiae fugit molestias temporibus consectetur, quidem qui ad delectus!" },
  ])
  const [doctors] = useState([{
    nome: 'Francesco Gialli',
    professione: 'Medico Chirurgo',
    zona: 'Napoli',
    attivo: true
  }, {
    nome: 'Puccino Verdi',
    professione: 'Psicologo Cognitivo Comportamentale',
    zona: 'Napoli',
    attivo: true
  }, {
    nome: 'Andrea Rossi',
    professione: 'Medico Generico',
    zona: 'Napoli',
    attivo: false
  }, {
    nome: 'Anna Violetto',
    professione: 'Medico Dermatologo',
    zona: 'Roma',
    attivo: true
  }, {
    nome: 'Francesco Gialli',
    professione: 'Medico Chirurgo',
    zona: 'Napoli',
    attivo: true
  }, {
    nome: 'Francesco Gialli',
    professione: 'Medico Chirurgo',
    zona: 'Napoli',
    attivo: true
  }, {
    nome: 'Francesco Gialli',
    professione: 'Medico Chirurgo',
    zona: 'Napoli',
    attivo: true
  }, {
    nome: 'Francesco Gialli',
    professione: 'Medico Chirurgo',
    zona: 'Napoli',
    attivo: true
  }, {
    nome: 'Francesco Gialli',
    professione: 'Medico Chirurgo',
    zona: 'Napoli',
    attivo: true
  }])

  const [story, setStory] = useState(null)
  const [inCall, setInCall] = useState(false)
  const [composeStory, setComposeStory] = useState(false)
  const [inChat, setInChat] = useState(false)
  // const [myStory, setMyStory] = useState(null)
  const [inCalendar, setInCalendar] = useState(false)
  const [date, setDate] = useState(new Date())

  useEffect(() => {
    window.watsonAssistantChatOptions = {
      integrationID: "b8aa1d36-987b-48a9-9c0c-7c79f00b4173", // The ID of this integration.
      region: "eu-gb", // The region your integration is hosted in.
      serviceInstanceID: "bc957e50-6842-4a6a-807e-b71c2e1e26a4", // The ID of your service instance.
      onLoad: function (instance) {
        instance.render();
        // const chatRegion = document.getElementById('WACWidget')
        document.getElementById('WACLauncher__Button').className = 'chat'


      }
    };
    setTimeout(function () {
      const t = document.createElement('script');
      t.src = "https://web-chat.global.assistant.watson.appdomain.cloud/loadWatsonAssistantChat.js";
      document.head.appendChild(t);
    });
    return () => {

    }
  }, [])

  // active ripple effect
  PrimeReact.ripple = true;

  return (
    <Router>

      <LoginDialog show={logged}
        handleLogin={() => setLogged(true)}
        handleRegister={() => setRegistering(true)}
        handleLostPassword={() => toastRef.current.show({ severity: 'success', summary: 'Password inoltrata', detail: 'Controlla la tua email', life: 3000 })}
      />

      <RegisterDialog show={registering}
        handleRegister={(choice) => {
          switch (choice) {
            case ACCEPT:
            case REFUSE:
            default:
              setRegistering(false)
          }
        }}
      />

      <Layout toastRef={toastRef} showContent={logged}>


        <Dialog header={story?.nome + " - " + story?.titolo} visible={story !== null} style={{ width: '80%' }} closable={true} onHide={() => setStory(null)}>{story?.storia}</Dialog>
        <Dialog header={() => (<p class="saving">Chiamata in corso<span>.</span><span>.</span><span>.</span></p>)} visible={inCall} style={{ width: '80%' }} closable={true} onHide={() => setInCall(false)}>Attendi che il professionista selezionato accetti la tua chiamata</Dialog>
        <Dialog header="Scegli uno slot orario" visible={inCalendar} style={{ width: '80%' }} closable={true} onHide={() => setInCalendar(false)}>
          <div className="p-d-flex p-flex-column p-jc-center p-ai-center">

            <Calendar showTime value={date} touchUI onChange={e => setDate(e.value)} showIcon></Calendar>
            <Button label="Richiedi appuntamento" className="p-button-raised p-button-rounded p-my-3" icon='pi pi-check' onClick={() => { setInCalendar(false); toastRef.current.show({ severity: 'success', summary: 'Richiesta inoltrata', detail: 'Il professionista ti contattera\' entro le prossime ore', life: 3000 }) }} />

          </div>
        </Dialog>
        <Dialog header="Racconta" visible={composeStory} style={{ width: '80%' }} closable={true} onHide={() => setComposeStory(false)}>
          {/* <Editor style={{ height: '320px' }} value={myStory} onTextChange={(e) => setMyStory(e.htmlValue)} /> */}
          TO-DO
        </Dialog>
        <Dialog header="Chat" visible={inChat} style={{ width: '80%' }} closable={true} onHide={() => setInChat(false)}>
          {/* <Editor style={{ height: '320px' }} value={myStory} onTextChange={(e) => setMyStory(e.htmlValue)} /> */}
          TO-DO
        </Dialog>


        <Switch>
          <Route path="/profile">
            <div className="p-col-12 
                        p-text-bold p-text-center
                        p-text-uppercase">Informazioni</div>
            {Object.keys(profile).map(k => (<React.Fragment>
              <div className="p-col-4 
                        p-text-bold p-text-right
                        p-text-capitalize">
                {k}
              </div>
              <div className="p-col-8 
                      p-pr-3">
                <div contentEditable

                  onChange={e => setProfile({ ...profile, [k]: e.target.value })}
                >{profile[k]}</div>
              </div>
            </React.Fragment>

            ))}

            <div className="p-col-12 
                        p-text-bold p-text-center
                        p-text-uppercase">Follow Up</div>
            <Divider align='center' />

            {

              followups.map(e => Object.keys(e).map(k => (<React.Fragment>
                <div className="p-col-4 
                            p-text-bold p-text-right
                            p-text-capitalize">
                  {k}
                </div>
                <div className="p-col-8 
                          p-pr-3">
                  <div contentEditable


                  >{e[k]}</div>
                </div>
              </React.Fragment>

              )

              )

              )
            }
            <Divider />

          </Route>
          <Route path="/share">
            <div className="p-col-12" style={{ overflow: 'auto', maxHeight: '90%' }}>
              {stories.map(e => (<React.Fragment>
                <Divider />
                <div className="p-d-flex p-flex-nowrap p-ai-center">
                  <Avatar image={User} shape='circle' size='large' />
                  <div className="p-text-nowrap p-text-truncate p-px-1 p-px-md-3">{e.nome}</div>
                  <div className="p-text-nowrap p-text-truncate p-text-light p-px-1 p-px-md-3">{e.eta} anni</div>
                  <div className="p-text-nowrap p-text-truncate p-text-light p-px-1 p-px-md-3">Stadio {e.stato}</div>
                  <div style={{ flexGrow: 1 }}></div>
                  <Button label="Leggi" className="p-button-raised p-button-rounded" onClick={ev => setStory(e)} />
                </div>
                <Divider />
              </React.Fragment>

              ))}
            </div>
            <Button label="Entra in chat" className="p-button-raised p-button-rounded" style={{ position: 'absolute', right: 20, bottom: 20 }} onClick={e => setInChat(true)} />
            <Button label="Scrivi la tua storia" className="p-button-raised p-button-rounded" style={{ position: 'absolute', left: 20, bottom: 20 }} onClick={e => setComposeStory(true)} />
          </Route>
          <Route path="/doctors">
            <div className="p-col-12">
              {doctors.map(e => (<React.Fragment>
                <Divider />
                <div className="p-d-flex p-flex-column p-flex-md-row p-flex-nowrap p-ai-center">
                  <div className="p-d-flex p-ai-center">
                    <Avatar image={Doc} shape='circle' size='large' />
                    <div className="p-text-nowrap p-text-truncate p-px-1 p-px-md-3">{e.nome}</div>
                    <div className={e.attivo ? 'stato-attivo' : 'stato-inattivo'}></div>
                  </div>
                  <div className="p-d-flex">
                    <div className="p-text-nowrap p-text-truncate p-text-light p-px-1 p-px-md-3">{e.professione} di {e.zona}</div>

                  </div>
                  <div style={{ flexGrow: 1 }}></div>
                  {e.attivo ?
                    <Button label="Chiama" className="p-button-raised p-button-rounded" icon='pi pi-video' onClick={e => setInCall(true)} /> :
                    <Button label="Prenota" className="p-button-raised p-button-rounded" icon='pi pi-calendar-plus' onClick={e => setInCalendar(true)} />
                  }
                </div>
                <Divider />
              </React.Fragment>

              ))}
            </div>
          </Route>
        </Switch>

        {logged ? <Redirect to={'/profile'} /> : null}

      </Layout>
    </Router>
  );
}

export default App;


